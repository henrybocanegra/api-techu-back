const URL_BASE      = '/techu-peru/v3/';
const baseMLabURL   = process.env.MLAB_COLLECTIONS;
const apikeyMLab    = "apiKey=" + process.env.MLAB_API_KEY;

const DEFAULT_PORT  = 3000;

function writeUserDataToFile(data){
  let fs = require('fs');
  let jsonUserData = JSON.stringify(data);
  fs.writeFile('./user.json', jsonUserData, 'utf8', function(err){
    if(err){
      console.err('Error al guardar archivo.');
    } else {
      console.log('Fichero guardado correctamente');
    }
  });
}

module.exports = {
  URL_BASE,
  baseMLabURL,
  apikeyMLab,
  DEFAULT_PORT,
  writeUserDataToFile
}
