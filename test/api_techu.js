var mocha = require("mocha"); // JavaScript Test Framework
var chai = require("chai"); // Aserciones
var chaihttp = require("chai-http"); // Peticiones HTTP
//Aumentamos la funcionalidad de chai, con este plugin para poder lanzar peticiones
//http
chai.use(chaihttp);
const should = chai.should;
const expect = chai.expect;

 describe('Test de API TechU - get users/:id',//Suite de test unitario
    function(){//Funcion manejadora de una suite
      it ('Prueba que la API devuelve el usuario 10:', //Test unitario
        function(done){
            chai.request('http://localhost:3000')
                .get('/techu-peru/v2/users/10')
                .end(
                  function(err, res){
                    console.log("Resquest has ended");
                    console.log(err);
                    expect(res.body).to.have.property('ID').to.be.equal(10);
                    expect(res).to.have.status(200);
                    done();
                  })
        })
    });

    describe('Test de API TechU - get users/:id',//Suite de test unitario
       function(){//Funcion manejadora de una suite
         it ('Prueba que la API devuelve el usuario 10:', //Test unitario
           function(done){
               chai.request('http://localhost:3000')
                   .get('/techu-peru/v2/users/10')
                   .end(
                     function(err, res){
                       console.log("Resquest has ended");
                       console.log(err);
                       expect(res.body).to.have.property('ID').to.be.equal(10);
                       expect(res).to.have.status(200);
                       done();
                     })
           })
       });
