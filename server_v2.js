const express = require('express');
const body_parser = require('body-parser');
const requestJSON = require('request-json');
const cors = require('cors');
require('dotenv').config();

// Controlers
var users = require('./controllers/users');
var accounts = require('./controllers/accounts');
var transactions = require('./controllers/transactions');
var auth = require('./controllers/auth');

const global = require('./global');
var app = express();
var port = process.env.PORT || global.DEFAULT_PORT;

app.use(body_parser.json());
app.use(cors());
app.options('*',cors());

app.listen(port, function(){
  console.log('Se escucha en puerto '+ port +'...');
});

//Petición GET de users
app.get(global.URL_BASE + 'users', users.getUsers);

//Petición GET de users con id
app.get(global.URL_BASE + 'users/:id', users.getUsersByID);

//Petición POST de users
app.post(global.URL_BASE + 'users', users.createUser);

//PUT users con parámetro 'id'
app.put(global.URL_BASE + 'users/:id', users.updateUser);

//Petición PUT con id de mLab (_id.$oid)
app.put(global.URL_BASE + 'usersmLab/:id',users.updateUserMlab);

//Petición DELETE de users con id
app.delete(global.URL_BASE + 'users/:id', users.deleteUser);

//Login
app.post(global.URL_BASE + 'login', auth.login);

//Logout
app.post(global.URL_BASE + 'logout/:id', auth.logout);

//GET de Accounts de User con id
app.get(global.URL_BASE + 'users/:id/accounts',accounts.getAccounts);

//POST de accounts
app.post(global.URL_BASE + 'users/:id/accounts',accounts.createAccount);

//PUT accounts con parámetro 'id'
app.put(global.URL_BASE + 'accounts/:id',
        accounts.updateAccount);

//GET de Transaction de Account con id
app.get(global.URL_BASE + 'accounts/:id/transactions',
        transactions.getTransactions);
