const express = require('express');
const body_parser = require('body-parser');
const global = require('./global');

var users = require('./controllers/users-json');
var auth = require('./controllers/auth-json');
var user_file = require('./data/user.json');
var app = express();
var port = process.env.PORT || 3000;

app.use(body_parser.json());

app.listen(port, function(){
  console.log('Se escucha en puerto '+ port +'...');
});

//Petición GET de users
app.get(global.URL_BASE + 'users', users.getUsers);

//Petición GET de users con ID
app.get(global.URL_BASE + 'users/:id', users.getUsersByID);

//Petición POST de users
app.post(global.URL_BASE + 'users',users.createUser);

//Petición PUT de users
app.put(global.URL_BASE + 'users/:id',users.updateUser);

//Petición DELETE de users
app.delete(global.URL_BASE + 'users/:id', users.deleteUser);

//Login
app.post(global.URL_BASE + 'login', auth.login);

//Logout
app.post(global.URL_BASE + 'logout/:id', auth.logout);
