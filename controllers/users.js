const global = require('../global');
const requestJSON = require('request-json');

//Lista usuarios
function getUsers(req, res){
    console.log("GET users");
    var httpClient = requestJSON.createClient(global.baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    var req_url = 'users?' + queryString + global.apikeyMLab;
    console.log(req_url);
    httpClient.get(req_url,
      function(err, respuestaMLab, body) {
         var response = {};
         if(err) {
             response = {"msg" : "Error obteniendo usuario."}
             res.status(500);
         } else {
           if(body.length > 0) {
             console.log(body.length);
             response = body;
           } else {
             response = {"msg" : "Ningún elemento 'user'."}
             res.status(404);
           }
         }
         res.send(response);
    });
}

//Get de users con ID
function getUsersByID(req, res){
  console.log("GET users con id=" + req.params.id);
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0,"password":0}&q={"id":'+req.params.id+'}&';
  var req_url = 'users?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     var response = {};
     if(err) {
         response = {"msg" : "Error obteniendo usuario."}
         res.status(500);
     } else {
       if(body.length > 0) {
         console.log(body.length);
         response = body[0];
       } else {
         response = {"msg" : "Ningún elemento 'user'."}
         res.status(404);
       }
     }
     res.send(response);
    });
}

//Create de users
function createUser(req, res){
  console.log("POST users");
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&c=true&';
  var req_url = 'users?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     if(!err) {
       var new_id = body + 1;
       var new_user = {
         "id": new_id,
         "first_name": req.body.first_name,
         "last_name": req.body.last_name,
         "email": req.body.email,
         "password": req.body.password
       }

       //Crea el usuario
       var req_url_post = 'users?' + global.apikeyMLab;
       httpClient.post(req_url_post, new_user,
         function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"msg" : "Error al crear usuario."}
              res.status(500);
          } else {
            console.log(body);
            response = {"msg" : "Usuario creado", "id":new_id};
          }
          res.send(response);
         });

     } else {
       response = {"msg" : "GET Users: Error mLab"}
       res.status(500);
       res.send(response);
     }
  });
}

//Update de users
function updateUser(req, res){
  var id = req.params.id;
  var clienteMlab = requestJSON.createClient(global.baseMLabURL);
  var queryStringID = 'q={"id":' + id + '}&';
  var req_url = 'users?'+ queryStringID + global.apikeyMLab;
  console.log(req_url);
  clienteMlab.get(req_url,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     var req_url_put = 'users?' + queryStringID + global.apikeyMLab;
     console.log("cambio: " +  cambio);
     clienteMlab.put(req_url_put, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
  });
}

//Update de user con id MLAB
function updateUserMlab(req, res){
  var id = parseInt(req.params.id);
  let userBody = req.body;
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  var req_url = 'users?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log(body);
      //Actualizo campos del usuario
      let updatedUser = {
        "id" : parseInt(req.body.id),
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      var req_url_put = 'users/' + response._id.$oid + '?' + global.apikeyMLab;
      httpClient.put(req_url_put, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }
          res.send(response);
        });
    });
}

//Delete de users
function deleteUser(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';

   var httpClient = requestJSON.createClient(global.baseMLabURL);
   var req_url = 'users?' +  queryStringID + global.apikeyMLab;
   httpClient.get(req_url,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       if(!error && respuesta){
         var req_url_del = "users/" + respuesta._id.$oid +'?'+ global.apikeyMLab;
         httpClient.delete(req_url_del,
           function(error, respuestaMLab,body){
             if(!error){
               res.send(body);
             } else {
               res.status(500).send({"msg":"Error en la operación DELETE"});
             }

         });
       } else {
         res.status(500).send({"msg":"Error en la operación DELETE"});
       }

   });
}

module.exports = {
  getUsers,
  getUsersByID,
  createUser,
  updateUser,
  updateUserMlab,
  deleteUser
}
