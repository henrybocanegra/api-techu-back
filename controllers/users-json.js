const global = require('../global');

var user_file = require('../data/user.json');

//Lista usuarios
//Soporta logueados y no Logueados
//slice(0) -> clona
function getUsers(req, res){
  let logged_users = [];
  let no_logged_users = [];
  let resp_array = [];
  let limit;

  for (let i = 0; i < user_file.length; i++) {
    if(user_file[i].logged) logged_users.push(user_file[i]);
    else no_logged_users.push(user_file[i]);
  }
  if(req.query.logged != undefined) //Logueados / No logueados
    resp_array = req.query.logged=='true' ?
        logged_users.slice(0) : no_logged_users.slice(0);
  else { //Cualquier otro caso enviá toda la lista
    resp_array = user_file.slice(0);
  }

  limit = req.query.limit || resp_array.length;
  console.log(limit);
  while(limit<resp_array.length){
    resp_array.pop();
  }
  res.send(resp_array);
}

//Get de users con ID
function getUsersByID(req, res){
  console.log(req.params);
  let pos = req.params.id - 1;
  let respuesta = (user_file[pos]==undefined) ?
      {"msg":"usuario no encontrado"} : user_file[pos];

  res.send(respuesta);
}

//Create de users
function createUser(req, res){
  console.log('Nuevo usuario:' + JSON.stringify(req.body));
  let new_id = user_file.length + 1;

  let new_user = {
    "id": new_id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };

  console.log('Crear: ' + JSON.stringify(new_user));
  user_file.push(new_user);
  global.writeUserDataToFile(user_file);
  console.log('Tam: ' + user_file.length);
  res.send(new_user);
}

//Update de users
function updateUser(req, res){
  console.log('Actualizar usuario:' + JSON.stringify(req.body));
  let user = req.body;
  let pos = req.params.id -1;

  if(user_file[pos] ==undefined)
    res.send({"msg":"Usuario no encontrado"});
  else {
    user_file[pos] = user;
    global.writeUserDataToFile(user_file);
    console.log('Valor actual: ' + JSON.stringify(user_file[pos]));
    res.send(user_file[pos]);
  }
}

//Delete de users
function deleteUser(req, res){
  console.log('Eliminar usuario:' + req.params.id);
  let pos = req.params.id -1;

  if(user_file[pos] != undefined && user_file[pos].ID == req.params.id){
    user_file.splice(pos,1);
    global.writeUserDataToFile(user_file);
    console.log('Tam: ' + user_file.length);
    res.send({"msg":"DELETE satisfactorio"});
  } else {
    res.send({"msg":"Usuario no existe"});
  }
}

module.exports = {
  getUsers,
  getUsersByID,
  createUser,
  updateUser,
  deleteUser
}
