const global = require('../global');
var user_file = require('../data/user.json');

//Login de usuario
function login (req, res){
  console.log('Login');
  console.log('email:' + req.body.email);
  console.log('password:' + req.body.password);
  let email = req.body.email;
  let password = req.body.password;
  let i=0;
  let found=false;
  if(email && password) {
    while(i<user_file.length && !found){
      if(user_file[i].email == email &&
        user_file[i].password== password){
        found=true;
        user_file[i].logged = true;
        //Persiste el archivo
        global.writeUserDataToFile(user_file);
      }
      i++;
    }
  }

  if(found){
    res.send({"msg":"Usuario logueado correcto", "ID" : i});
  }
  else {
    res.send({"msg":"Email / password incorrectos."});
  }
}

//Logout de usuario
function logout(req, res){
  console.log('Logout');
  console.log('ID:' + req.params.id);
  let pos = req.params.id -1;
  if(user_file[pos] != undefined
    && user_file[pos].ID == req.params.id
    && user_file[pos].logged){
    delete user_file[pos].logged;
    //Persiste el archivo
    global.writeUserDataToFile(user_file);
    res.send({"msg":"Logout correcto ", "ID": user_file[pos].ID});

  } else {
    res.send({"msg":"Logout incorrecto"});
  }
}

module.exports = {
  login,
  logout
}
