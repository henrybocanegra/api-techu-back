const global = require('../global');
const requestJSON = require('request-json');

//Lista accounts
function getAccounts(req, res){
  console.log("GET accounts de users con id=" + req.params.id);
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&q={"id_user":'+req.params.id+'}&';
  var req_url = 'accounts?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     var response = {};
     if(err) {
         response = {"msg" : "Error obteniendo accounts."}
         res.status(500);
     } else {
       if(body.length > 0) {
         console.log(body.length);
         response = body;
       } else {
         response = {"msg" : "Ningún elemento 'account'."}
         res.status(404);
       }
     }
     res.send(response);
    });
}

//Create de account
function createAccount(req, res){
  console.log("Create account");
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  var queryString = 'f={"_id":0}&s={"id":-1}&l=1&';
  var req_url = 'accounts?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
       console.log(JSON.stringify(body));
       if(!err && body.length==1) {
         var new_id = body[0].id + 1;
         var new_account = req.body;
         new_account.id = new_id;
         new_account.id_user = parseInt(req.params.id);

         //Crea el usuario
         var req_url_post = 'accounts?' + global.apikeyMLab;
         httpClient.post(req_url_post, new_account,
           function(err, respuestaMLab, body) {
            var response = {};
            if(err) {
                console.log(err);
                response = {"msg" : "Error al crear cuenta."}
                res.status(500);
            } else {
              response = {"msg" : "Cuenta creada", "id":new_id};
            }
            res.send(response);
           });
       } else {
         res.status(500).send({"msg" : "GET Users: Error mLab"});
       }
  });
}

function updateAccount(req, res){
  var id = parseInt(req.params.id);
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  var req_url = 'accounts?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body){
      let updAccount = body[0];
      var str_upd = `{"$set":${JSON.stringify(req.body)}}`;
      var req_url_put = 'accounts/' + updAccount._id.$oid + '?' + global.apikeyMLab;

      httpClient.put(req_url_put, JSON.parse(str_upd),
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando cuenta."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Cuenta actualizada correctamente."
              }
              res.status(200);
            }
          }
          res.send(response);
        });
    });
}

module.exports = {
  getAccounts,
  createAccount,
  updateAccount
}
