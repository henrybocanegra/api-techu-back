const global = require('../global');
const requestJSON = require('request-json');

//Lista de transactions
function getTransactions(req, res){
  console.log("GET transaction de account con id=" + req.params.id);
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&q={"id":'+req.params.id+'}&';
  var req_url = 'transactions?' + queryString + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     var response = {};
     if(err) {
         response = {"msg" : "Error obteniendo transactions."}
         res.status(500);
     } else {
       if(body.length > 0) {
         console.log(body.length);
         response = body;
       } else {
         response = {"msg" : "Ningún elemento 'transaction'."}
         res.status(404);
       }
     }
     res.send(response);
    });
}

module.exports = {
  getTransactions
}
