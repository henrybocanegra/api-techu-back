const global = require('../global');
const requestJSON = require('request-json');

//Login
function login(req, res){
  console.log("Login");
  let email = req.body.email;
  let password = req.body.password;
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  var queryStringID = 'f={"password":0}&q={"email":"' + email + '","password":"' + password + '"}&l=1&';
  var req_url = 'users?'+ queryStringID + global.apikeyMLab;
  console.log(req_url);
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     if(err) {
       res.status(500);
       res.send({"msg" : "Error en el login"});
     } else {
       if(body.length == 1 && !body[0].logged) {
         var str_login = '{"$set":{"logged":true}}';
         var req_url_put = 'users/' + body[0]._id.$oid + '?' + global.apikeyMLab;
         console.log(req_url_put);
         httpClient.put(req_url_put, JSON.parse(str_login),
           function(error, respuestaMLab, putbody) {
             if(!error && putbody.id){
               delete body[0]._id;
               res.send({"msg":"Login correcto","user":body[0]});
             } else {
               console.log(error);
               res.status(500).send({"msg":"Error en el login"});
             }
         });

       } else {
         res.status(404);
         res.send({"msg" : "Login incorrecto"});
       }
     }
  });

}

//Logout
function logout(req, res){
  console.log("Logout");
  let id = req.params.id;
  var httpClient = requestJSON.createClient(global.baseMLabURL);
  var queryStringID = 'q={"id":' + id + '}&';
  var req_url = 'users?'+ queryStringID + global.apikeyMLab;
  httpClient.get(req_url,
    function(err, respuestaMLab, body) {
     if(err) {
       res.status(500);
       res.send({"msg" : "Error en el logout"});
     } else {
       if(body.length == 1 && body[0].logged) {
         var str_logout = '{"$unset":{"logged":""}}';
         var req_url_put = 'users/' + body[0]._id.$oid + '?' + global.apikeyMLab;
         console.log(req_url_put);
         httpClient.put(req_url_put, JSON.parse(str_logout),
           function(error, respuestaMLab, body) {
             if(!error && body.id){
               res.send({"msg":"Logout correcto","id":body.id});
             } else {
               console.log(error);
               res.status(500).send({"msg":"Error en el Logout"});
             }
         });

       } else {
         res.status(404);
         res.send({"msg" : "Usuario no logueado"});
       }
     }
  });

}

module.exports = {
  login,
  logout
}
